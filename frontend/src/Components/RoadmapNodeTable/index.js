import React from 'react';
import '../node.css';
import "../LessonDisplay"
import CourseDisplay from '../LessonDisplay';

class TableToPrint extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemList: props.itemList
        };
    }
    render() {
        const trnglist = {
            color: "#010733",
            backgroundColor: "white",
            textAlign: "left",
            verticalAlign: "middle",
            border: ".5em solid white",
            margin: "auto"
        }

        const table = {
            backgroundColor: "white",
            width: "100%",
            paddingLeft: "10px 0px",
            borderRadius: "5px",
            display: "flex",
            marginTop: "5px"
        }

        const tbody = {
            marginLeft: "5px"
        }
        
        return (
            <div style={this.props.style}>
                <table style={table}><tbody style={tbody}>
                    {this.state.itemList.map(trngItem => (
                        <tr style={trnglist} key={trngItem.trngId} >
                            <td>
                                <CourseDisplay course={trngItem}/>
                            </td>
                        </tr>
                    ))}
                </tbody></table>
            </div>
        );
    }
}

export default TableToPrint