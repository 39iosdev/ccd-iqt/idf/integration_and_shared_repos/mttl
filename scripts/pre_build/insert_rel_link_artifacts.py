#!/usr/bin/python3

import os
import sys
import json
import requests
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
import mongo_helpers

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))
    
filename = os.path.splitext(os.path.basename(__file__))[0]

error = 0
log_obj = {}
artifact_url = "-/jobs/artifacts/master/raw/data.rel-link.json?job=SaveJsons"


def error_message(repo:str, ksat: str, msg: str, err=1):
    global error, log_obj
    if error != 1: error = err

    if repo in log_obj:
        log_obj[repo].update({ ksat: msg })
    else:
        log_obj.update({ 
            repo: {
                ksat: msg
            }
        })

def request_artifact(url):
    """
    Helper function to perform HTTP requests
    """
    job_url = f'{url.replace(".git", "")}/{artifact_url}'
    req = None
    data = None
    print(f'Pulling {job_url}')
    try: 
        req = requests.get(f'{job_url}')
        data = req.json()
    except Exception as e:
        err = f'ERROR: {e} in {job_url}'
        if req:
            err += f' with status code: {req.status_code}'
        print(err)
        exit(1)
    return data

# rl = rel_link
def insert_rel_link_info(collection, rl_data, url, mat_key, rl_key):
    """
    This function will take the rel-link data and insert the info into the repective KSAT
    """
    tmp_mat_key = mat_key # hold onto the original mat_key value
    url = url.replace('.git', '')
    for rl_dict in rl_data:
        for req_grp in rl_dict['KSATs']:
            mat_key = tmp_mat_key # reassign mat_key in case it was changed from 'ojt'
            # req_grp[0] is the KSAT, req_grp[1] is the prof
            if len(req_grp) == 0:
                continue
            # fill in the prof with blank if it does not exist 
            if len(req_grp) == 1:
                req_grp.append('')
            else: # remove any white space
                req_grp[1] = req_grp[1].replace(' ', '')
            # create error message if prof is blank
            if len(req_grp[1]) == 0 and 'T' not in req_grp[0]:
                error_message(url, req_grp[0], '{ksat} proficiency in "{path}" missing!'.format(ksat = req_grp[0], path = rl_dict['REL_PATH']))

            # change the mat_key if 'ojt' in name
            if 'ojt' in rl_dict['NAME']:
                mat_key = 'eval'

            REL_PATH = os.path.dirname(rl_dict['REL_PATH'])
            mongo_helpers.find_id_and_append_array(collection, req_grp[0], mat_key, {
                '_id': rl_dict['TRN_EVL_ID'],
                'name': rl_dict['NAME'],
                'proficiency': req_grp[1],
                'network': rl_dict['NETWORK'],
                'url': f'{url}/-/tree/master/{REL_PATH}'
            }, upsert=False)

def main():
    global error
    json_list = ["WORK-ROLES.json", "SPECIALIZATIONS.json"]
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    for path in json_list: # for WORK-ROLES.json and SPECIALZIATIONS.json
        with open(path, 'r') as datafile:
            url_data = json.load(datafile)
        
        for key in url_data.keys(): # for all training and eval urls
            for url in url_data[key]['trn-rel-link-urls']:
                insert_rel_link_info(reqs, request_artifact(url), url, 'training', key) # request and insert rel-link data
            for url in url_data[key]['evl-rel-link-urls']:
                insert_rel_link_info(reqs, request_artifact(url), url, 'eval', key)

    if error:
        with open(f'{filename}.json', 'w') as log_file:
           json.dump(log_obj, log_file, sort_keys=False, indent=4)  
        print(f'There are errors present. Read the \'{filename}.json\' artifact file')

if __name__ == "__main__":
    main()