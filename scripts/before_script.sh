#!/bin/bash

if [ -z ${MONGO_HOST} ]; then
    MONGO_HOST=localhost;
fi

mongo --host $MONGO_HOST --eval 'db.dropDatabase()' mttl

# import all requirement files into mongodb requirements colleciton
if [ -d "requirements" ]; then
    mongo --host $MONGO_HOST --eval 'db.requirements.createIndex({"ksat_id": 1}, {unique: true})' mttl
    for f in requirements/*.json
    do
        until mongoimport --host $MONGO_HOST --db mttl --collection requirements --file $f --jsonArray; do sleep 1; done
    done
fi

# import all rel-link files into mongodb rel_links collection
if [ -d "rel-links/training" ]; then
    for f in rel-links/training/*.rel-links.json
    do
        mongoimport --host $MONGO_HOST --db mttl --collection rel_links --file $f --jsonArray
    done
fi
if [ -d "rel-links/eval" ]; then
    for f in rel-links/eval/*.rel-links.json
    do
        mongoimport --host $MONGO_HOST --db mttl --collection rel_links --file $f --jsonArray
    done
fi

# import all work-role files into mongodb work_roles colleciton
if [ -d "work-roles" ]; then
    for f in work-roles/*.json
    do
        until mongoimport --host $MONGO_HOST --db mttl --collection work_roles --file $f --jsonArray; do sleep 1; done
    done
fi